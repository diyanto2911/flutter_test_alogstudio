import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_test_algostudio/app/remote_data/image_remote_data.dart';
import 'package:flutter_test_algostudio/core/models/image_models.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';

class ProviderMemGenerator extends ChangeNotifier{


  ImageModels? _imageModels;

  XFile? file;



  ImageModels? get imageModels => _imageModels;

  set imageModels(ImageModels? value) {
    _imageModels = value;
    notifyListeners();
  }



  void getImageRemoteData()async{
    final res=await ImageRemoteData().getImageModel();
    imageModels=res;

  }

  Future<void> onRefreshing()async{
    imageModels=null;
    getImageRemoteData();
  }


  Future getImageGalery()async{
    await askPermission();
    final ImagePicker _picker = ImagePicker();
    final XFile? image = await _picker.pickImage(source: ImageSource.gallery);
    file=image;
    notifyListeners();
  }

  askPermission() async {
    await Permission.photos.request();
    await Permission.storage.request();
  }


}