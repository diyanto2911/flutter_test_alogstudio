import 'package:dio/dio.dart';
import 'package:flutter_test_algostudio/core/models/image_models.dart';
import 'package:flutter_test_algostudio/core/network/dio_service.dart';

class ImageRemoteData {
  final dio = DioService.getInstance();


  Future<ImageModels?> getImageModel()async{
      try{
        final res=await dio.get('get_memes');
        return ImageModels.fromJson(res.data);
      }on DioError catch(e){
        return null;
      }
  }

}
