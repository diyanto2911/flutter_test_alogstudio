import 'package:flutter/material.dart';
import 'package:flutter_test_algostudio/app/ui/mem_generator/list_image_mem.dart';
import 'package:flutter_test_algostudio/core/provider/providers.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class MemGeneratorCore extends StatelessWidget {
  const MemGeneratorCore({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: Providers.depedencyList,
        child: GetMaterialApp(
          title: "Mim Generator",
          theme: ThemeData(primaryColor: Colors.blue),
          themeMode: ThemeMode.light,
          debugShowCheckedModeBanner: false,
          home: const ListImageMemView(),

        ));
  }
}
