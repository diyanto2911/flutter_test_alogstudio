import 'dart:io';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

import 'package:flutter_test_algostudio/app/provider/provider_memgenerator.dart';
import 'package:get/get.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';

class MimGeneratorView extends StatefulWidget {
  final String url;
  const MimGeneratorView({Key? key, required this.url}) : super(key: key);

  @override
  State<MimGeneratorView> createState() => _MimGeneratorViewState();
}

class _MimGeneratorViewState extends State<MimGeneratorView> {
  final TextEditingController _controllerText = TextEditingController();
  String? headerText;

  final GlobalKey globalKey = GlobalKey();


  Future convertWidgetToImage() async {
    RenderRepaintBoundary? repaintBoundary =
    globalKey.currentContext!.findRenderObject() as RenderRepaintBoundary?;
    ui.Image boxImage = await repaintBoundary!.toImage(pixelRatio: 1);
    ByteData? byteData =
    await boxImage.toByteData(format: ui.ImageByteFormat.png);
    Uint8List uint8list = byteData!.buffer.asUint8List();

    return uint8list;
  }


 Future saveToTemporary()async{
    Uint8List uint8list = await convertWidgetToImage();

    if(GetPlatform.isAndroid){
      await Permission.manageExternalStorage.request();
      await Permission.storage.request();
      final output = await getApplicationDocumentsDirectory();
      final fileName="/${DateTime.now().microsecondsSinceEpoch}_mem.png";
      final filePath=output.path+fileName;
      final imageFile=await File(filePath).create();
      await imageFile.writeAsBytes(uint8list, flush: true);
      Future.delayed(const Duration(seconds: 1), () {

        Share.shareFiles([filePath], text: fileName);
      });
    }else{
      await Permission.manageExternalStorage.request();
      await Permission.storage.request();
      final output = await getApplicationDocumentsDirectory();
      final fileName="/${DateTime.now().microsecondsSinceEpoch}_mem.png";
      final filePath=output.path+fileName;
      final imageFile=await File(filePath).create();
      await imageFile.writeAsBytes(uint8list, flush: true);
      Future.delayed(const Duration(seconds: 1), () {

        Share.shareFiles([filePath], text: fileName);
      });
    }


  }

    takeScreenshot() async {

    Uint8List uint8list = await convertWidgetToImage();

    var appDocDir =   GetPlatform.isAndroid ? await  getTemporaryDirectory() : await getApplicationDocumentsDirectory();
    String savePath = appDocDir.path + "/${DateTime.now().microsecondsSinceEpoch}_mem.png";
    final imageFile=await File(savePath).create();
    await imageFile.writeAsBytes(uint8list, flush: true);
    final result = await ImageGallerySaver.saveFile(savePath);
    print(result);
    Get.snackbar(
      'File Berhasil Didownload',
      result['filePath'],
      backgroundColor: Colors.green,
      colorText: Colors.white
    );

    
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Mim Generator'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Consumer<ProviderMemGenerator>(
          builder: (context, data, _) {
            return RepaintBoundary(
              key: globalKey,
              child: SizedBox(
                height: 500,
                width: 500,
                child: Column(
                  children: [
                    if (headerText != null)
                      Text(
                        headerText.toString().toUpperCase(),
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                    const SizedBox(
                      height: 10,
                    ),
                    Stack(
                      children: [
                        CachedNetworkImage(
                          imageUrl: widget.url,
                        ),
                        if (data.file != null)
                          Positioned(
                            top: 10,
                            left: 10,
                            child: Image.file(
                              File(data.file!.path),
                              height: 200,
                              width: 200,
                            ),
                          )
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
      persistentFooterButtons: [
        Consumer<ProviderMemGenerator>(builder: (context, data, _) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              data.file == null
                  ? TextButton.icon(
                      onPressed: () => Provider.of<ProviderMemGenerator>(
                              context,
                              listen: false)
                          .getImageGalery(),
                      icon: const Icon(Icons.photo),
                      label: const Text('Add Logo'))
                  : TextButton(
                      onPressed: () => takeScreenshot(),
                      child: const Text('Simpan'),
                    ),
              const SizedBox(
                width: 10,
              ),
              headerText == null
                  ? TextButton(
                      onPressed: () => showDilog(),
                      child: const Text('Add Text'),
                    )
                  : TextButton(
                      onPressed: () => showBottomSheetShare(),
                      child: const Text('Share'),
                    ),
            ],
          );
        }),
      ],
    );
  }

  Future showDilog() async {
    Get.defaultDialog(
        title: 'Add Text',
        content: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              autofocus: true,
              controller: _controllerText,
            ),
            const SizedBox(
              height: 30,
            ),
            Center(
                child: TextButton(
                    onPressed: () {
                      setState(() {
                        headerText = _controllerText.text;
                        _controllerText.clear();
                      });
                      Get.back();
                    },
                    child: const Text('Tambahkan Text')))
          ],
        ));
  }


  Future showBottomSheetShare()async{
    Get.bottomSheet(Container(
      height: 200,
      decoration:const BoxDecoration(
        borderRadius:  BorderRadius.only(topLeft: Radius.circular(10),topRight: Radius.circular(10),
        ),
            color: Colors.white
      ),
      width: double.infinity,
      child: ListView(
        padding: const EdgeInsets.all(10),
        children: [
          ListTile(
            onTap: ()async{
              saveToTemporary();
            },
            title: const Text('Share To Facebook'),

          ),
          ListTile(
            onTap: ()async{
             saveToTemporary();
            },
            title:const  Text('Share To Twitter'),

          )
        ],
      ),
    ));
  }
}
