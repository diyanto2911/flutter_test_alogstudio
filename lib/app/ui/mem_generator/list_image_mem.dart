import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test_algostudio/app/provider/provider_memgenerator.dart';
import 'package:flutter_test_algostudio/app/ui/mem_generator/mim_generator.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class ListImageMemView extends StatefulWidget {
  const ListImageMemView({Key? key}) : super(key: key);

  @override
  State<ListImageMemView> createState() => _ListImageMemViewState();
}

class _ListImageMemViewState extends State<ListImageMemView> {
  @override
  void initState() {
    Provider.of<ProviderMemGenerator>(context, listen: false)
        .getImageRemoteData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Mim Generator',
          style: TextStyle(
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: Consumer<ProviderMemGenerator>(builder: (context,data,_){
        if(data.imageModels==null) {
          return  const Center(
            child: CircularProgressIndicator(),
          );
        }else{
          if(data.imageModels?.data!.memes==null){
            return const Center(
              child: Text('Data Image Kosong',style: TextStyle(fontWeight: FontWeight.bold),),
            );
          }else{
         return   RefreshIndicator(
           onRefresh: data.onRefreshing,
           child: MediaQuery.removePadding(
                  context: context,
                  child: GridView.builder(
                      gridDelegate:
                      const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                      itemCount: data.imageModels?.data!.memes?.length,
                      itemBuilder: (c, i) {
                        var mem=data.imageModels?.data!.memes![i];
                        return InkWell(
                          onTap: ()=> Get.to(MimGeneratorView(url: mem!.url!)),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12)
                            ),
                              padding: const EdgeInsets.all(5),
                              margin: const EdgeInsets.all(5),
                              child: CachedNetworkImage(imageUrl: mem!.url!,
                              fit: BoxFit.cover,)),
                        );
                      })),
         );
          }
        }
      },)
    );
  }
}
