import 'package:flutter_test_algostudio/app/provider/provider_memgenerator.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class Providers {


  static final List<SingleChildWidget> depedencyList = [
    ChangeNotifierProvider<ProviderMemGenerator>(
      create: (_) => ProviderMemGenerator(),
    ),

  ];
}
