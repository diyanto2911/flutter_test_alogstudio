import 'dart:convert';

ImageModels imageModelsFromJson(String str) => ImageModels.fromJson(json.decode(str));
String imageModelsToJson(ImageModels data) => json.encode(data.toJson());
class ImageModels {
  ImageModels({
      this.success, 
      this.data,});

  ImageModels.fromJson(dynamic json) {
    success = json['success'];
    data = json['data'] != null ? Data.fromJson(json['data']) : null;
  }
  bool? success;
  Data? data;
ImageModels copyWith({  bool? success,
  Data? data,
}) => ImageModels(  success: success ?? this.success,
  data: data ?? this.data,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['success'] = success;
    if (data != null) {
      map['data'] = data?.toJson();
    }
    return map;
  }

}

Data dataFromJson(String str) => Data.fromJson(json.decode(str));
String dataToJson(Data data) => json.encode(data.toJson());
class Data {
  Data({
      this.memes,});

  Data.fromJson(dynamic json) {
    if (json['memes'] != null) {
      memes = [];
      json['memes'].forEach((v) {
        memes?.add(Memes.fromJson(v));
      });
    }
  }
  List<Memes>? memes;
Data copyWith({  List<Memes>? memes,
}) => Data(  memes: memes ?? this.memes,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (memes != null) {
      map['memes'] = memes?.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

Memes memesFromJson(String str) => Memes.fromJson(json.decode(str));
String memesToJson(Memes data) => json.encode(data.toJson());
class Memes {
  Memes({
      this.id, 
      this.name, 
      this.url, 
      this.width, 
      this.height, 
      this.boxCount,});

  Memes.fromJson(dynamic json) {
    id = json['id'];
    name = json['name'];
    url = json['url'];
    width = json['width'];
    height = json['height'];
    boxCount = json['box_count'];
  }
  String? id;
  String? name;
  String? url;
  int? width;
  int? height;
  int? boxCount;
Memes copyWith({  String? id,
  String? name,
  String? url,
  int? width,
  int? height,
  int? boxCount,
}) => Memes(  id: id ?? this.id,
  name: name ?? this.name,
  url: url ?? this.url,
  width: width ?? this.width,
  height: height ?? this.height,
  boxCount: boxCount ?? this.boxCount,
);
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['name'] = name;
    map['url'] = url;
    map['width'] = width;
    map['height'] = height;
    map['box_count'] = boxCount;
    return map;
  }

}